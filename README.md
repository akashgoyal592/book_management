# RoadMap to test Book Management project:

Actions:

1. Clone the repository.
2. Run the SpringbootappApplication.java file (in com.happay.book_management package) as Spring boot App (default server port 8080).


-----------------------------------------------------------------------------------------------

# Create Tables : Go to  

	http://localhost:8080/h2-console/
	
	
Credentials to login to h2-console:-
	
			url -> jdbc:h2:~/test
			ClassName -> org.h2.Driver
			userName -> sa
			password -> sa
		
	
1. Creating Book Table-
	
	Drop table book;
	
	Create table Book(
	bookId varchar(256) Not Null,
	title varchar(256),
	authorId varchar(256),
	publisher varchar(256),
	publishDate varchar(256),
	categoryId varchar(256),
	price Real,
	soldCount int
	);

2. Creating Author Table-

	Drop table Author;
	
	CREATE TABLE Author(
	AuthorID varchar(256) Not Null,
	AuthorName varchar(256),
	PhoneNumber varchar(10),
	BirthDate varchar(256),
	DeathDate varchar(256)
	);
	
3. Creating Category Table-

	Drop table Category;
	
	Create table Category(
	id varchar(256) Not Null,
	categoryName varchar(256)
	);
	
Note:- The same SQL Statements are also present in schema.sql file (Under src/main/resources)
-----------------------------------------------------------------------------------------------

# Know your server:

1. addAnAuthor -

Post Request - http://localhost:8080/happayBookManagement/addAnAuthor - Expecting Json data-{authorId,authorName,phoneNumber,birthDate,deathDate}

Assumptions:-

(1). Date format should be yyyy-mm-dd (Example if month or day is 2 then it should be enter as 02 otherwise date format will be inValid).
(2). authorId can not be Null.
(3). Phone number length should be less then or equal to 10.

JSON request Body:-

Test:- 

{
    "authorId":"Author1",
    "authorName":"JK Rowling",
    "phoneNumber":"12341234",
    "birthDate":"1978-10-14",
    "deathDate":null
}

Response Expected:- Author added successfully

Test:-

{
    "authorId":"Author2",
    "authorName":"Dan Brown",
    "phoneNumber":null,
    "birthDate":"1978-10-41",
    "deathDate":null
}

Test:-

Response Expected:- Date is InValid

{
    "authorId":"Author3",
    "authorName":"Dan Simmons",
    "phoneNumber":null,
    "birthDate":"1972-12-31",
    "deathDate":null
}

Response Expected:- Author added successfully

2. addBookToCatalog - 

Post Request - http://localhost:8080/happayBookManagement/addBookToCatalog - Expecting Json data-{bookId,title,authorId,publisher,publishDate,categoryId,price,soldCount}

Assumptions:-

(1). Date format should be yyyy-mm-dd (Example if month or day is 2 then it should be entered as 02 otherwise date format will be inValid).
(2). bookId can not be Null.


JSON request Body:-

Test:- 

{
	"bookId":"Book 2",
	"title":"Adventure Book with Magic 1",
	"authorId":"Author1",
	"publisher":"Gutenberg",
	"publishDate":"2000-01-01",
	"categoryId":"category1",
	"price":200,
	"soldCount":200000
}

Response Expected:- Book added successfully to the Catalog



3. addCategory - 

Post Request - http://localhost:8080/happayBookManagement/addCategory - Expecting JSON data-{id,categoryName}

Assumptions:-

(1). categoryId can not be Null.


JSON request Body:-

Example:-
{
	"id":"category1",
	"categoryName":"fiction"
}

Response Expected:- Category added successfully


4. getListOfCategories - 

Get Request - http://localhost:8080/happayBookManagement/getListOfCategories


Example:-
		http://localhost:8080/happayBookManagement/getListOfCategories
		
Response Expected:- 

[ {
  "id" : "category1",
  "categoryName" : "fiction"
} ]


5. getAllAuthorName - 

Get Request - http://localhost:8080/happayBookManagement/getAllAuthorName

Example:-
		http://localhost:8080/happayBookManagement/getAllAuthorName
		
Response Expected:- 

["Dan Simmons", "JK Rowling" ]


6. getMostBooksSoldByAuthor - 

Get Request - http://localhost:8080/happayBookManagement/getMostBooksSoldByAuthor/{authorId}

Example:-
	   http://localhost:8080/happayBookManagement/getMostBooksSoldByAuthor/author1

Response Expected:- 

[ "Adventure Book with Magic 1" ]


6. getMostBooksSoldByCategory - 

Get Request - http://localhost:8080/happayBookManagement/getMostBooksSoldByCategory/{categoryId}

Example:-
	  http://localhost:8080/happayBookManagement/getMostBooksSoldByCategory/category1

Response Expected:- 

[ "Adventure Book with Magic 1" ]


7. searchBook - Get Request - http://localhost:8080/happayBookManagement/searchBook/{Searching keyword}

Assumptions:-
	
(1). We can search Book by giving partial Book or Author Name;
	
(2). To search a book by Book Name and Author Name both as a single request: separate them with ":".

Example:- 

Search Book by Book name:-

http://localhost:8080/happayBookManagement/searchBook/book

Search Book by Author name:-

http://localhost:8080/happayBookManagement/searchBook/author

Search Book By Both :- (Suppose Book Name is book 1 and Author Name is Author your request Object will either be book:author OR author:book) 


http://localhost:8080/happayBookManagement/searchBook/book : author
or
http://localhost:8080/happayBookManagement/searchBook/author : book 1


8. getBooksByAuthor - Get Request - http://localhost:8080/happayBookManagement/getBooksByAuthor/{authorId}

Example:- 

http://localhost:8080/happayBookManagement/getBooksByAuthor/author1


Stack:

-Spring Boot Application
-Spring MVC
-Spring Jdbc
-Maven
-Spring h2 database(In Memory)

-----------------------------------------------------------------------------------------------

