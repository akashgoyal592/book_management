Drop table book;
Create table Book(
bookId varchar(256) Not Null,
title varchar(256),
authorId varchar(256),
publisher varchar(256),
publishDate varchar(256),
categoryId varchar(256),
price Real,
soldCount int
);

Drop table Author;

CREATE TABLE Author
(
AuthorID varchar(256) Not Null,
AuthorName varchar(256),
PhoneNumber varchar(10),
BirthDate varchar(256),
DeathDate varchar(256)
);



Drop table Category;
Create table Category(
id varchar(256) Not Null,
categoryName varchar(256)
);
