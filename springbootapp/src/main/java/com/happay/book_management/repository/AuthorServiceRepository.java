package com.happay.book_management.repository;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.happay.book_management.model.Author;

@Repository
public class AuthorServiceRepository {


	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	private static String INSERT_STATEMENT = "INSERT INTO Author(AuthorID,AuthorName,PhoneNumber,BirthDate, DeathDate) VALUES (?,?,?,?,?)" ;

	public void saveAll(Author authorDetails) {
			
			jdbcTemplate.batchUpdate(INSERT_STATEMENT, new BatchPreparedStatementSetter() {
				
				@Override
				public void setValues(PreparedStatement ps, int i) throws SQLException {
					// TODO Auto-generated method stub
					
					ps.setString(1,authorDetails.getAuthorId());
					ps.setString(2, authorDetails.getAuthorName());
					ps.setString(3, authorDetails.getPhoneNumber());
					ps.setString(4, authorDetails.getBirthDate());
					ps.setString(5, authorDetails.getDeathDate());
				}
				
				@Override
				public int getBatchSize() {
					// TODO Auto-generated method stub
					return 1;
				}
			});
			
		}
	
		public List<String> findAllAuthors() {
				
				return jdbcTemplate.query("SELECT DISTINCT authorName from Author", new RowMapper<String>(){  
				   
					@Override
					public String mapRow(ResultSet rs, int rowNum) throws SQLException {
						// TODO Auto-generated method stub
						
						String authorName;
						authorName=rs.getString("AuthorName");
						return authorName;
					}  
				    });  
			}
		
		public List<String> authorAlreadyExist() {
			
			return jdbcTemplate.query("SELECT authorName from Author", new RowMapper<String>(){  
			   
				@Override
				public String mapRow(ResultSet rs, int rowNum) throws SQLException {
					// TODO Auto-generated method stub
					
					String authorName;
					authorName=rs.getString("AuthorName");
					return authorName;
				}  
			    });  
		}
		
		
}
