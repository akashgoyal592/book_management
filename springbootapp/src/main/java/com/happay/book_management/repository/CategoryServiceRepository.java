package com.happay.book_management.repository;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
  

import com.happay.book_management.model.Category;

@Repository
public class CategoryServiceRepository {
	
	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	private static String INSERT_STATEMENT = "INSERT INTO Category(id,categoryName) VALUES(?,?)";
	
	public void saveAll(Category categoryDetails)
	{
		
		jdbcTemplate.batchUpdate(INSERT_STATEMENT, new BatchPreparedStatementSetter() {
			
			@Override
			public void setValues(PreparedStatement ps, int i) throws SQLException {
				// TODO Auto-generated method stub
				
				ps.setString(1, categoryDetails.getId());
				ps.setString(2, categoryDetails.getCategoryName());
				
			}
			
			@Override
			public int getBatchSize() {
				// TODO Auto-generated method stub
				return 1;
			}
		});
		

	}
	
	
	public List<Category> findAllCategories() {
		
		return jdbcTemplate.query("select *from Category", new RowMapper<Category>(){  
		   
			@Override
			public Category mapRow(ResultSet rs, int rowNum) throws SQLException {
				// TODO Auto-generated method stub
				
				Category category=new Category();
				category.setId(rs.getString(1));
				category.setCategoryName(rs.getString(2));
				return category;
			}  
		    });  
	}
	
	public List<String> categoryAlreadyExist() {
		
		return jdbcTemplate.query("SELECT DISTINCT categoryName from Category", new RowMapper<String>(){  
		   
			@Override
			public String mapRow(ResultSet rs, int rowNum) throws SQLException {
				// TODO Auto-generated method stub
				
				String categoryName;
				categoryName=rs.getString("categoryName");
				return categoryName;
			}  
		    });  
	}
	
	
	
}
