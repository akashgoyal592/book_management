package com.happay.book_management.repository;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashSet;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.happay.book_management.model.Book;

@Repository
public class BookServiceRepository {
	

	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	private static String INSERT_STATEMENT = "INSERT INTO Book(bookId,title,authorId,publisher,publishDate,categoryId,price,soldCount) VALUES(?,?,?,?,?,?,?,?)";
	
	public void saveAll(Book bookDetails)
	{
		
		jdbcTemplate.batchUpdate(INSERT_STATEMENT, new BatchPreparedStatementSetter() {
			
			@Override
			public void setValues(PreparedStatement ps, int i) throws SQLException {
				// TODO Auto-generated method stub
				ps.setString(1, bookDetails.getBookId());
				ps.setString(2, bookDetails.getTitle());
				ps.setString(3, bookDetails.getAuthorId());
				ps.setString(4, bookDetails.getPublisher());
				ps.setString(5, bookDetails.getPublishDate());
				ps.setString(6, bookDetails.getCategoryId());
				ps.setDouble(7, bookDetails.getPrice());
				ps.setInt(8, bookDetails.getSoldCount());
			}
			
			@Override
			public int getBatchSize() {
				// TODO Auto-generated method stub
				return 1;
			}
		});
			
	}
	
	public List<String> findMostBooksSoldByAuthor(String authorId) {
			
		authorId=authorId.toLowerCase();
		authorId="'"+authorId+"'";
		return jdbcTemplate.query("SELECT title from Book where lower(authorId) Like" +authorId + " AND soldCount=(SELECT max(soldCount) from Book where lower(authorId)="+ authorId + ")", new RowMapper<String>(){  
			   
			@Override
			public String mapRow(ResultSet rs, int rowNum) throws SQLException {
				// TODO Auto-generated method stub
				
				String title;
				title=rs.getString("title");
				return title;
			}  
		    }); 
		}
	
		public List<String> findMostBooksSoldByCategory(String categoryId) {
			
			categoryId=categoryId.toLowerCase();
			categoryId="'"+categoryId+"'";
			return jdbcTemplate.query("SELECT title from Book where lower(categoryId) LIKE " +categoryId + " AND soldCount=(SELECT max(soldCount) from Book where lower(categoryId) LIKE"+ categoryId + ")", new RowMapper<String>(){  
				   
				@Override
				public String mapRow(ResultSet rs, int rowNum) throws SQLException {
					// TODO Auto-generated method stub
					
					String title;
					title=rs.getString("title");
					return title;
				}  
			    }); 
			}
		
		public HashSet<Book> findBook(String[] keyList) {
			
			HashSet<Book> result=new HashSet<>();
			
			if(keyList.length==1)
			{
				String keyword="'%"+keyList[0].trim()+"%'";
				String sqlForTitle= "SELECT * FROM BOOK where lower(title) Like "+keyword;
				
				String sqlForAuthor= "SELECT * FROM BOOK where authorId in (SELECT authorId FROM Author where lower(authorName) Like "+keyword+")";
				

				List<Book> bookDetailsForTitle= queryForSearchBook(sqlForTitle);

				List<Book> bookDetailsForAuthor= queryForSearchBook(sqlForAuthor);
				
				result.addAll(bookDetailsForTitle);
				result.addAll(bookDetailsForAuthor);
			}
			else
			{

				String keyword1="'%"+keyList[0].trim()+"%'";
				String keyword2="'%"+keyList[1].trim()+"%'";
				
				String sqlForTitleAuthor= "SELECT *from book where authorId in (SELECT authorId FROM Author where lower(authorName) Like "+ keyword1+ ") AND lower(title) Like " +keyword2;
				String sqlForAuthorTitle= "SELECT *from book where authorId in (SELECT authorId FROM Author where lower(authorName) Like "+ keyword2+ ") AND lower(title) Like " +keyword1;
				
				List<Book> bookDetailsForTitle= queryForSearchBook(sqlForTitleAuthor);

				List<Book> bookDetailsForAuthor= queryForSearchBook(sqlForAuthorTitle);
				
				result.addAll(bookDetailsForTitle);
				result.addAll(bookDetailsForAuthor);
			}
			return result;
			
		}
		
		public List<String> findBooksByAuthor(int authorId) {
					
					return jdbcTemplate.query("SELECT title from Book where authorId =" + authorId , new RowMapper<String>(){  
						   
						@Override
						public String mapRow(ResultSet rs, int rowNum) throws SQLException {
							// TODO Auto-generated method stub
							
							String title;
							title=rs.getString("title");
							return title;
						}  
					    }); 
					}
		
		
		public List<String> bookAlreadyExist() {
			
			return jdbcTemplate.query("SELECT title from Book", new RowMapper<String>(){  
			   
				@Override
				public String mapRow(ResultSet rs, int rowNum) throws SQLException {
					// TODO Auto-generated method stub
					
					String title;
					title=rs.getString("title");
					return title;
				}  
			    });  
		}
		
		
		private List<Book> queryForSearchBook(String sql){
			
			return jdbcTemplate.query(sql, new RowMapper<Book>(){  
				   
				@Override
				public Book mapRow(ResultSet rs, int rowNum) throws SQLException {
					// TODO Auto-generated method stub
										
					Book book=new Book();
					book.setAuthorId(rs.getString("authorId"));
					book.setBookId(rs.getString("bookId"));
					book.setTitle(rs.getString("title"));
					book.setCategoryId(rs.getString("categoryId"));
					book.setPrice(rs.getDouble("price"));
					book.setPublishDate(rs.getString("publishDate"));
					book.setPublisher(rs.getString("publisher"));
					book.setSoldCount(rs.getInt("soldCount"));
					
					
					return book;
				}  
			    }); 
		}
		
		
}
