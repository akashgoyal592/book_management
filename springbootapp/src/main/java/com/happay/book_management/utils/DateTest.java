package com.happay.book_management.utils;

public class DateTest {

	public static boolean checkDate(String date){
		
		if(date==null)
		{
			return true;
		}
		String[] dateSplit=date.split("-");
		
		int year;
		try {
			year=Integer.parseInt(dateSplit[0]);
		}
		catch(Exception e) {
			return false;
		}
		
		int month;
		try {
			month=Integer.parseInt(dateSplit[1]);
		}
		catch(Exception e) {
			return false;
		}
		
		int days;
		try {
			days=Integer.parseInt(dateSplit[2]);
		}
		catch(Exception e) {
			return false;
		}
		
		if(dateSplit[0].length()!=4 || dateSplit[1].length()!=2 || dateSplit[2].length()!=2)
		{
			return false;
		}
		
		if(month<1 || month>12)
		{
			return false;
		}
		
		if(month==1 || month==3|| month==5|| month==7|| month==8|| month==10||month==12)
		{
			if(days<1 || days>31)
			{
				return false;
			}
		}
		else if(month==2)
		{
			if(checkLeapYear(year))
			{
				if(days<1 || days>29)
				{
					return false;
				}
			}
			else
			{
				if(days<1 || days>28)
				{
					return false;
				}
			}
		}
		else {
			if(days<1 || days>30)
			{
				return false;
			}
		}
		
		return true;
	}
	
	public static boolean checkLeapYear(int year) {
		
		if (year % 4 == 0) {

		      if (year % 100 == 0) {

		        if (year % 400 == 0)
		        {
		        	return true;
		        }
		        else
		        {
		         return false;
		        }
		          
		      }
		      else
		      {
		    	  return true;
		      }
		        
		    }
		
		return false;
		
	}
}
