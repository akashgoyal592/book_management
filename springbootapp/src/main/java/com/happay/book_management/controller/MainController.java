package com.happay.book_management.controller;

import java.util.HashSet;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.happay.book_management.exception.AuthorAlreadyExistsException;
import com.happay.book_management.exception.BookAlreadyExistsException;
import com.happay.book_management.exception.CategoryAlreadyExistsException;
import com.happay.book_management.model.Author;
import com.happay.book_management.model.Book;
import com.happay.book_management.model.Category;
import com.happay.book_management.service.AuthorService;
import com.happay.book_management.service.BookService;
import com.happay.book_management.service.CategoryService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
 

@RestController
@RequestMapping("/happayBookManagement")
public class MainController {
	
	@Autowired
	private AuthorService authorService;
	
	@Autowired
	private BookService bookService;
	
	@Autowired
	private CategoryService categoryService;
	
	ObjectMapper objectMapper = new ObjectMapper().configure(SerializationFeature.INDENT_OUTPUT, true);
	
	@PostMapping("/addAnAuthor")
	public String addAnAuthor(@RequestBody Author authorDetails) throws AuthorAlreadyExistsException {
		
		String response;
		if(authorService.authorAlreadyExist(authorDetails))
		{
			response = "Author Already Exists in Database";
		}
		else
		{
			response =authorService.addAnAuthor(authorDetails);
		}
		
		return response;
	}
	
	@PostMapping("/addBookToCatalog")
	public String addBookToCatalog(@RequestBody Book bookDetails) throws BookAlreadyExistsException
	{
		String response;
		if(bookService.bookAlreadyExist(bookDetails))
		{
			response = "Book Already Exists in Database";
		}
		else
		{
			response =bookService.addBookToCatalog(bookDetails);
		}
		
		return response;
		
	}
	
	@PostMapping("/addCategory")
	public String addCategory(@RequestBody Category categoryDetails) throws CategoryAlreadyExistsException
	{
		String response;
		if(categoryService.categoryAlreadyExist(categoryDetails))
		{
			response = "Category Already Exists in Database";
		}
		else
		{
			response =categoryService.addCategory(categoryDetails);
		}
		return response;
	}
	
	@GetMapping("/getListOfCategories")
	public Object getListOfCategories() throws JsonProcessingException {
		
		List<Category> result= categoryService.findAllCategories();
		  
		String listToJson = objectMapper.writeValueAsString(result);
		
        return listToJson;
	}
	
	@GetMapping("/getAllAuthorName")
	public Object getListOfAuthors() throws JsonProcessingException {
		
		List<String> result= authorService.findAllAuthors();
		  
		String listToJson = objectMapper.writeValueAsString(result);
		
        return listToJson;
	}
	
	@GetMapping("/getMostBooksSoldByAuthor/{id}")
	public Object getMostBooksSoldByAuthor(@PathVariable("id") String authorId) throws JsonProcessingException{
		
		List<String> result= bookService.getMostBooksSoldByAuthor(authorId);
		  
		String listToJson = objectMapper.writeValueAsString(result);
		
        return listToJson;
	}
	
	@GetMapping("/getMostBooksSoldByCategory/{id}")
	public Object getMostBooksSoldByCategory(@PathVariable("id") String categoryId) throws JsonProcessingException{
		
		List<String> result= bookService.getMostBooksSoldByCategory(categoryId);
		  
		String listToJson = objectMapper.writeValueAsString(result);
		
        return listToJson;
	}
	
	@GetMapping("/searchBook/{key}")
	public Object searchBook(@PathVariable("key") String key) throws JsonProcessingException{
		
		HashSet<Book> result= bookService.searchBook(key);
		  
		String listToJson = objectMapper.writeValueAsString(result);
		
        return listToJson;
	}
	
	@GetMapping("/getBooksByAuthor/{id}")
	public Object getBooksByAuthor(@PathVariable("id") int id) throws JsonProcessingException{
		
		List<String> result= bookService.getBooksByAuthor(id);
		  
		String listToJson = objectMapper.writeValueAsString(result);
		
        return listToJson;
	}
	

}
