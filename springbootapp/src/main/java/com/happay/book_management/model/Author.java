package com.happay.book_management.model;

public class Author {
	
	private String authorId;
	private String authorName;
	private String phoneNumber;
	private String birthDate;
	private String deathDate;
	public Author() {
		super();
	}
	public Author(String authorId, String authorName, String phoneNumber, String birthDate, String deathDate) {
		super();
		this.authorId = authorId;
		this.authorName = authorName;
		this.phoneNumber = phoneNumber;
		this.birthDate = birthDate;
		this.deathDate = deathDate;
	}
	public String getAuthorId() {
		return authorId;
	}
	public void setAuthorId(String authorId) {
		this.authorId = authorId;
	}
	public String getAuthorName() {
		return authorName;
	}
	public void setAuthorName(String authorName) {
		this.authorName = authorName;
	}
	public String getPhoneNumber() {
		return phoneNumber;
	}
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	public String getBirthDate() {
		return birthDate;
	}
	public void setBirthDate(String birthDate) {
		this.birthDate = birthDate;
	}
	public String getDeathDate() {
		return deathDate;
	}
	public void setDeathDate(String deathDate) {
		this.deathDate = deathDate;
	}
	@Override
	public String toString() {
		return "Author [authorId=" + authorId + ", authorName=" + authorName + ", phoneNumber=" + phoneNumber
				+ ", birthDate=" + birthDate + ", deathDate=" + deathDate + "]";
	}

}
