package com.happay.book_management.model;

public class Book {

	private String bookId;
	private String title;
	private String authorId;
	private String publisher;
	private String publishDate;
	private String categoryId;
	private double price;
	private int soldCount;
	
	
	public Book() {
		super();
	}


	public Book(String bookId, String title, String authorId, String publisher, String publishDate, String categoryId,
			double price, int soldCount) {
		super();
		this.bookId = bookId;
		this.title = title;
		this.authorId = authorId;
		this.publisher = publisher;
		this.publishDate = publishDate;
		this.categoryId = categoryId;
		this.price = price;
		this.soldCount = soldCount;
	}


	public String getBookId() {
		return bookId;
	}


	public void setBookId(String bookId) {
		this.bookId = bookId;
	}


	public String getTitle() {
		return title;
	}


	public void setTitle(String title) {
		this.title = title;
	}


	public String getAuthorId() {
		return authorId;
	}


	public void setAuthorId(String authorId) {
		this.authorId = authorId;
	}


	public String getPublisher() {
		return publisher;
	}


	public void setPublisher(String publisher) {
		this.publisher = publisher;
	}


	public String getPublishDate() {
		return publishDate;
	}


	public void setPublishDate(String publishDate) {
		this.publishDate = publishDate;
	}


	public String getCategoryId() {
		return categoryId;
	}


	public void setCategoryId(String categoryId) {
		this.categoryId = categoryId;
	}


	public double getPrice() {
		return price;
	}


	public void setPrice(double price) {
		this.price = price;
	}


	public int getSoldCount() {
		return soldCount;
	}


	public void setSoldCount(int soldCount) {
		this.soldCount = soldCount;
	}


	@Override
	public String toString() {
		return "Book [bookId=" + bookId + ", title=" + title + ", authorId=" + authorId + ", publisher=" + publisher
				+ ", publishDate=" + publishDate + ", categoryId=" + categoryId + ", price=" + price + ", soldCount="
				+ soldCount + "]";
	}
	
	
	
	
	
	
	
}
