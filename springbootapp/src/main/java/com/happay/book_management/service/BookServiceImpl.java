package com.happay.book_management.service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.happay.book_management.exception.BookAlreadyExistsException;
import com.happay.book_management.model.Book;
import com.happay.book_management.repository.BookServiceRepository;
import com.happay.book_management.utils.DateTest;

@Repository
public class BookServiceImpl implements BookService{
	
	@Autowired
	private BookServiceRepository bookServiceRepository;

	@Override
	public String addBookToCatalog(Book bookDetails) throws BookAlreadyExistsException {
		// TODO Auto-generated method stub
		
		String result;
		
		String publishDate=bookDetails.getPublishDate();

		
		if(bookDetails.getBookId()!=null)
		{
			if(DateTest.checkDate(publishDate))
			{
				try {
					bookServiceRepository.saveAll(bookDetails);
					result="Book added to the Catalog";
				}
				catch(Exception e){
					result="Book could not be added to the Catalog";
					e.printStackTrace();
				}
			}
			else {
				result="Date is InValid";
			}
			
		}
		else
		{
			result="Book Id can not be NULL : Book could not be added to the Catalog";
		}
		return result;
		
		
	}

	@Override
	public List<String> getMostBooksSoldByAuthor(String authorId) {
		// TODO Auto-generated method stub
		List<String> result=new ArrayList<>();
		
		try {
			result= bookServiceRepository.findMostBooksSoldByAuthor(authorId);
		}
		catch(Exception e){
			e.printStackTrace();
		}
		return result;
	}

	@Override
	public List<String> getMostBooksSoldByCategory(String categoryId) {
		// TODO Auto-generated method stub
		List<String> result=new ArrayList<>();
		
		try {
			result= bookServiceRepository.findMostBooksSoldByCategory(categoryId);
		}
		catch(Exception e){
			e.printStackTrace();
		}
		return result;
	}

	@Override
	public HashSet<Book> searchBook(String key) {
		// TODO Auto-generated method stub
		
		HashSet<Book> result=new HashSet<>();
		key=key.toLowerCase();
		String[] keyList=key.split(":");
		
		try {
			result= bookServiceRepository.findBook(keyList);
		}
		catch(Exception e){
			e.printStackTrace();
		}
		return result;
	}

	@Override
	public List<String> getBooksByAuthor(int authorId) {
		// TODO Auto-generated method stub
		List<String> result=new ArrayList<>();
		
		try {
			result= bookServiceRepository.findBooksByAuthor(authorId);
		}
		catch(Exception e){
			e.printStackTrace();
		}
		return result;
	}

	@Override
	public boolean bookAlreadyExist(Book bookDetails) {
		// TODO Auto-generated method stub
		boolean result= false;
		
		String checkTitle=bookDetails.getTitle();
		
		List<String> bookTitles=new ArrayList<>();
		
		try {
			bookTitles= bookServiceRepository.bookAlreadyExist();
			
			for(String bookTitle:bookTitles)
			{
				if(checkTitle.equals(bookTitle))
				{
					result=true;
				}
			}
		}
		catch(Exception e){
			e.printStackTrace();
		}
		return result;
	}
}
