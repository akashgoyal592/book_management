package com.happay.book_management.service;

import java.util.HashSet;
import java.util.List;

import com.happay.book_management.exception.BookAlreadyExistsException;
import com.happay.book_management.model.Book;

public interface BookService {
	
	public String addBookToCatalog(Book bookDetails) throws BookAlreadyExistsException;

	public List<String> getMostBooksSoldByAuthor(String authorId);
	
	public List<String> getMostBooksSoldByCategory(String categoryId);
	
	public HashSet<Book> searchBook(String key);
	
	public List<String> getBooksByAuthor(int id);
	
	public boolean bookAlreadyExist(Book bookDetails);
	

}
