package com.happay.book_management.service;

import java.util.List;

import com.happay.book_management.exception.AuthorAlreadyExistsException;
import com.happay.book_management.model.Author;

public interface AuthorService {
	
	public String addAnAuthor(Author authorDetails) throws AuthorAlreadyExistsException;
	
	public List<String> findAllAuthors();
	
	public boolean authorAlreadyExist(Author authorDetails);

}
