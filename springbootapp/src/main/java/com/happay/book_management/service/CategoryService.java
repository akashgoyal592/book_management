package com.happay.book_management.service;

import java.util.List;

import com.happay.book_management.exception.CategoryAlreadyExistsException;
import com.happay.book_management.model.Category;

public interface CategoryService {
	
	public String addCategory(Category categoryDetails) throws CategoryAlreadyExistsException;
	
	public List<Category> findAllCategories();

	public boolean categoryAlreadyExist(Category categoryDetails);
}
