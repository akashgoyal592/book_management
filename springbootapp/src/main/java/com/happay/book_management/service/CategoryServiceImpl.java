package com.happay.book_management.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.happay.book_management.exception.CategoryAlreadyExistsException;
import com.happay.book_management.model.Category;
import com.happay.book_management.repository.CategoryServiceRepository;

@Repository
public class CategoryServiceImpl implements CategoryService{
	
	@Autowired
	private CategoryServiceRepository categoryServiceRepository;

	@Override
	public String addCategory(Category categoryDetails) throws CategoryAlreadyExistsException{
		// TODO Auto-generated method stub
		String result;
		
		if(categoryDetails.getId()!=null)
		{
			try {
				categoryServiceRepository.saveAll(categoryDetails);
				result="Category added successfully";
			}
			catch(Exception e){
				result="Category could not be added";
				e.printStackTrace();
			}
		}
		else {
			result="Category ID can not be null : Category could not be added";
		}
		return result;
	}

	@Override
	public List<Category> findAllCategories() {
		// TODO Auto-generated method stub
		List<Category> result=new ArrayList<>();
		
		try {
			result= categoryServiceRepository.findAllCategories();
		}
		catch(Exception e){
			e.printStackTrace();
		}
		return result;
	}

	@Override
	public boolean categoryAlreadyExist(Category categoryDetails) {
		// TODO Auto-generated method stub
		
		boolean result= false;
		
		String checkName=categoryDetails.getCategoryName();
		
		List<String> categoryNames=new ArrayList<>();
		
		try {
			categoryNames= categoryServiceRepository.categoryAlreadyExist();
			
			for(String categoryName:categoryNames)
			{
				if(checkName.equals(categoryName))
				{
					result=true;
				}
			}
		}
		catch(Exception e){
			e.printStackTrace();
		}
		return result;
	}
	
	

}
