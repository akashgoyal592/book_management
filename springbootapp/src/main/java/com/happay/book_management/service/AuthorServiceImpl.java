package com.happay.book_management.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.happay.book_management.exception.AuthorAlreadyExistsException;
import com.happay.book_management.model.Author;
import com.happay.book_management.repository.AuthorServiceRepository;
import com.happay.book_management.utils.DateTest;


@Repository
public class AuthorServiceImpl implements AuthorService{
	
	@Autowired
	private AuthorServiceRepository authorServiceRepository;
	
	@Override
	public String addAnAuthor(Author authorDetails) throws AuthorAlreadyExistsException {
		
		String result;
		
		String birthDate=authorDetails.getBirthDate();

		String deathDate=authorDetails.getDeathDate();
		
		if(authorDetails.getAuthorId()!=null)
		{
			if(DateTest.checkDate(birthDate) && DateTest.checkDate(deathDate)) 
			{
				
				try {
					authorServiceRepository.saveAll(authorDetails);
					result="Author added successfully";
				}
				catch(Exception e){
					
					result="Author could not be added";
					e.printStackTrace();
				}
			
			}
			else
			{
				result="Date is InValid";
			}
		}
		else
		{
			result="Author Id can not be null : Author could not be added";
		}
		
		return result;
	}

	@Override
	public List<String> findAllAuthors() {
		// TODO Auto-generated method stub
		List<String> result=new ArrayList<>();
		
		try {
			result= authorServiceRepository.findAllAuthors();
		}
		catch(Exception e){
			e.printStackTrace();
		}
		return result;
	}
	
	@Override
	public boolean authorAlreadyExist(Author authorDetails) {
		// TODO Auto-generated method stub
		boolean result= false;
		
		String checkName=authorDetails.getAuthorName();
		
		List<String> authorNames=new ArrayList<>();
		
		try {
			authorNames= authorServiceRepository.authorAlreadyExist();
			
			for(String authorName:authorNames)
			{
				if(checkName.equals(authorName))
				{
					result=true;
				}
			}
		}
		catch(Exception e){
			e.printStackTrace();
		}
		return result;
	}
	
	
}
